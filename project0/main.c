/* 
 * CIS 415 Project 0
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "anagram.h"

void readstdio(FILE *in_stream, FILE *out_stream){

    // Create buffer and Anagram List objects.
    char buffer[2048];
    struct AnagramList *cur_anagrams;
    cur_anagrams = NULL;

    // Read from stdin or file.
    while (fgets(buffer, sizeof(buffer), in_stream)){
        // For Linux based stripping newline.
        strtok(buffer, "\n");
        // For Windows stripping return.
        strtok(buffer, "\r");
        // Add word to Anagram List.
        AddWordAList(&cur_anagrams, buffer);
    }

    // Print the finished Anagram List.
    PrintAList(out_stream, cur_anagrams);
    // Free the Anagram List.
    FreeAList(&cur_anagrams);
}

int main(int argc, const char *argv[]){

    // Create File Objects and default to stdin and stdout.
    FILE *in_stream;
    FILE *out_stream;
    in_stream = stdin;
    out_stream = stdout;

    // Exit if bad parameters.
    if (argc > 3){
        exit(0);
    }
    // If there is input parameter.
    if (argc > 1){
        in_stream = fopen(argv[1], "r");
    }
    // If there is output parameter after input.
    if (argc > 2){
        out_stream = fopen(argv[2], "w");
    }
    //Check if files exists, if not, exit.
    if (in_stream == NULL || out_stream == NULL){
        exit(0);
    }

    // Proceed to readlines.
    readstdio(in_stream, out_stream);

    // Close IO streams.
    fclose(in_stream);
    fclose(out_stream);
    
    return 0;
}
