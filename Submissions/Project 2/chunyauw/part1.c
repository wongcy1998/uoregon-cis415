/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>

#include "Project2.h"

int main(int arc, char* argv[]){

    struct TopicEntry newPost = {"", ""}, newPost2 = {"", ""}, retriever = {"", ""};
    
    TopicQueue mainQueue;
    initQueue(&mainQueue, 20);
    fprintf(stderr, "New Queue initialised to size of: 20\n");

    fill_queue(&mainQueue);

    fprintf(stderr, "Printing Queue\n");
    print_queue(&mainQueue);

    fprintf(stderr, "Enqueue success? %d\n", enqueue(&mainQueue, newPost));
    fprintf(stderr, "Enqueue success? %d\n", enqueue(&mainQueue, newPost2));

    fprintf(stderr, "Emptying Queue, 0 for success: %d\n", empty_queue(&mainQueue));
    fprintf(stderr, "Filling in queue\n");
    fill_queue(&mainQueue);

    fprintf(stderr, "Dequeue success? %d\n", dequeue(&mainQueue));
    fprintf(stderr, "Dequeue success? %d\n", dequeue(&mainQueue));

    fprintf(stderr, "Reading next entry of 100, response: %lld\n", getentry(&mainQueue, 100, &retriever));
    fprintf(stderr, "Reading next entry of 0, response: %lld\n", getentry(&mainQueue, 0, &retriever));
    
    delete_topic_queue(&mainQueue);

    return 0;
}
