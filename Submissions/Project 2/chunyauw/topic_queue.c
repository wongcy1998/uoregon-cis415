/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "Project2.h"
#include "thread_safe_bounded_queue.h"

void initQueue(TopicQueue *queue, int size){
    queue->size = size;
    queue->entry_counter = 0;
    queue->ts_bq = TS_BB_MallocBoundedQueue(size);
}

int enqueue(TopicQueue *queue, struct TopicEntry item){
    struct TopicEntry *P_ITEM = malloc(sizeof(struct TopicEntry));
    *P_ITEM = item;
    P_ITEM->entrynum = queue->entry_counter;
    gettimeofday(&(P_ITEM->timestamp), NULL);
    if (TS_BB_TryEnqueue(queue->ts_bq, P_ITEM) < 0){
        fprintf(stderr, "<!> ENQUEUE ERROR! <!>\n");
        free(P_ITEM);
        return 0;
    } else {
        queue->entry_counter += 1;
        return 1;
    }
}

int dequeue(TopicQueue *queue){
    struct TopicEntry *freeing = TS_BB_GetItem(queue->ts_bq, TS_BB_GetBack(queue->ts_bq));
    if (TS_BB_TryDequeue(queue->ts_bq, TS_BB_GetBack(queue->ts_bq)) != 1){
        fprintf(stderr, "<!> DEQUEUE ERROR! <!>\n");
        return 0;
    }
    free(freeing);
    return 1;
}

long long getentry(TopicQueue *queue, long long ID, struct TopicEntry *entry){
    if (TS_BB_IsEmpty(queue->ts_bq) == 1){
        return -1;
    } else if (TS_BB_IsIdValid(queue->ts_bq, ID) == 1){
        struct TopicEntry *temporary = TS_BB_GetItem(queue->ts_bq, ID);
        *entry = *temporary;
        return ID;
    } else if (ID < TS_BB_GetBack(queue->ts_bq)){
        struct TopicEntry *temporary = TS_BB_GetItem(queue->ts_bq, TS_BB_GetBack(queue->ts_bq));
        *entry = *temporary;
        return TS_BB_GetBack(queue->ts_bq);
    } else {
        fprintf(stderr, "<!> Requested entry is not posted yet! <!>\n");
        return -1;
    }
}

int print_queue(TopicQueue *queue){
    long long i, head = TS_BB_GetFront(queue->ts_bq), tail = TS_BB_GetBack(queue->ts_bq);
    for (i = tail; i < head; i++){
        struct TopicEntry *print_holder = TS_BB_GetItem(queue->ts_bq, i);
        fprintf(stderr, "Got Entry: %lld\n", i);
        fprintf(stderr, "\tEntryNum: %d\n", print_holder->entrynum);
        fprintf(stderr, "\tID: %d\n", print_holder->pubID);
        fprintf(stderr, "\tURL: %s\n", print_holder->photoURL);
        fprintf(stderr, "\tCaption: %s\n", print_holder->photoCaption);
    }
    return 1;
}

void fill_queue(TopicQueue *queue){
    int i;
    for (i = 0; i < queue->size; i++){
        struct TopicEntry entry = {"ABC", "XYZ"};
        enqueue(queue, entry);
    }
}

int empty_queue(TopicQueue *queue){
    while(TS_BB_IsEmpty(queue->ts_bq) == 0){
        dequeue(queue);
    }
    return 0;
}

void delete_topic_queue(TopicQueue *queue){
    empty_queue(queue);
    TS_BB_FreeBoundedQueue(queue->ts_bq);
}
