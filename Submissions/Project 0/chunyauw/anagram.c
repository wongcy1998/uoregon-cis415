/* 
 * CIS 415 Project 0
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file is my own work, except the following:
 *      Quicksort for Char Arrays:
 *          https://stackoverflow.com/questions/23147569/using-qsort-for-character-array-in-c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "anagram.h"

int cmpfn(const void *a, const void *b){
    return *(char*)a - *(char*)b;
}

struct StringList *MallocSList(char *word){

    // Memory Allocation.
    struct StringList *SList = malloc(sizeof(struct StringList));

    // Elements
    SList->Word = strdup(word);
    SList->Next = NULL;

    return SList;
}

void AppendSList(struct StringList **head, struct StringList *node){

    // Create traversing node.
    struct StringList *s_holder = *head;

    // Traverse when node is not null.
    while (s_holder){
        // If current node's string matches.
        if (strcmp(s_holder->Word, node->Word) == 0){
            break;
        }
        // If at end of list, and not found.
        if (s_holder->Next == NULL){
            s_holder->Next = node;
        }
        // Proceed to next node, when not found.
        s_holder = s_holder->Next;
    }
}

void FreeSList(struct StringList **node){
    // Create traversing node.
    struct StringList *f_s_holder;

    // While node is not null, free.
    while (*node){
        f_s_holder = *node;
        // Zero out memory.
        memset((*node)->Word, 0, sizeof(*(*node)->Word));
        free((*node)->Word);
        *node = (*node)->Next;
        free(f_s_holder);
    }
}

void PrintSList(FILE *file,struct StringList *node){

    // Create traversing node.
    struct StringList *holder = node;

    // While holder is not null, print.
    while (holder){
        fprintf(file, "\t%s\n" , holder->Word);
        holder = holder->Next;
    }
}

int SListCount(struct StringList *node){

    // Create counter and traversing node.
    int counter = 0;
    struct StringList *copy = node;

    // Start counting.
    if (copy->Word != NULL){
        counter++;
        while (copy->Next != NULL){
            counter++;
            copy = copy->Next;
        }
    }
    return counter;
}

struct AnagramList* MallocAList(char *word){

    // Memory Allocations.
    struct AnagramList *AList = malloc(sizeof(struct AnagramList));

    // Elements
    AList->Anagram = strdup(word);
    AList->Next = NULL;
    AList->Words = NULL;
    return AList;
}

void FreeAList(struct AnagramList **node){

    // Create traversing node.
    struct AnagramList *f_a_holder;

    // Free while node is not null.
    while (*node){
        f_a_holder = *node;
        // Zero out memory.
        memset((*node)->Anagram, 0, sizeof(*(*node)->Anagram));
        free((*node)->Anagram);
        *node = (*node)->Next;
        FreeSList(&f_a_holder->Words);
        free(f_a_holder);
    }
}

void PrintAList(FILE *file,struct AnagramList *node){

    // Create a traversing node.
    struct AnagramList *holder = node;

    // Print while not at end of list.
    while(holder){
        if (SListCount(holder->Words) > 1){
            fprintf(file, "%s:%d\n", holder->Anagram, SListCount(holder->Words));
            PrintSList(file, holder->Words);
        }
        holder = holder->Next;
    }
}

void AddWordAList(struct AnagramList **node, char *word){

    // Length of word.
    int len = strlen(word), i;

    // To lower case and Quick Sort.
    char *lower_word = strdup(word);
    for (i = 0; i < len; i++){
        lower_word[i] = tolower(word[i]);
    }
    qsort(lower_word, strlen(lower_word), sizeof(char), cmpfn);

    // Create a traversing node.
    struct AnagramList *a_holder = *node;

    // Checker for if *node is NULL.
    if (*node){
        // While there is next element in Anagram List.
        while (a_holder){
            // If current node of anagrams matches.
            if (strcmp(a_holder->Anagram, lower_word) == 0){
                AppendSList(&a_holder->Words, MallocSList(word));
                break;
            }
            // If at end of list and anagram not found.
            if (a_holder->Next == NULL){
                a_holder->Next = MallocAList(lower_word);
                a_holder->Next->Words = MallocSList(word);
                break;
            }
            // Goto next node.
            a_holder = a_holder->Next;
        }
    }
    // If AnagramList is new.
    else {
        (*node) = MallocAList(lower_word);
        (*node)->Words = MallocSList(word);
    }
    // Free the strdup malloc
    free(lower_word);
}
