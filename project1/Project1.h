#ifndef PROJECT1_H
#define PROJECT1_H
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

///////////
// PART1 //
///////////

enum PCBState
{
    NotStarted,
    Running,
    Paused,
    Exited
};

// Structure for PCB.
struct PCB
{
    pid_t PID;
    char *cmd;
    char **args;
    int argc;
    int HasExited;
    enum PCBState State;
    int UserTime;
    int SystemTime;
    int ScheduleTime;
};

// The MCP launcher for processes.
void MCP_LAUNCHER();

// Function to wait until all processes are finished.
void MCP_WAIT();

// Readline from file for sum of lines.
void readsum(FILE *in_stream);

// Readline from file to input.
void readio(FILE *in_stream);

// Memory allocate the database.
void malloc_lines(struct PCB **mein_PCB);

// Freeing the memory of database.
void free_db();

///////////
// PART2 //
///////////

// Function to send designated signals to all.
void MCP_FIRESIG(int sgnl);

// Signal handler for SIGUSR1.
void SIGUSR1_HANDLER(int sgnl);

///////////
// PART3 //
///////////

// Signal handler for SIGALRM.
void SIGALRM_HANDLER(int sgnl);

// Signal handler for SIGCHLD.
void SIGCHLD_HANDLER(int sgnl);

// Signal handler for SIGUSR2.
void SIGUSR2_HANDLER(int sgnl);

// Check if all processes finished.
int MCP_HASEXITED();

// Timer function to invoke SIGALRM.
void MCP_TIMER(int custom_time);

///////////
// PART4 //
///////////

// Manages the entire process info output.
void MCP_INFO_MANAGER();

// Fetches info of proc-documents.
void MCP_FETCH_INFO(int pid, int procindex);

///////////
// PART5 //
///////////

void MCP_SCHEDULE_TIME(int index, int cutime, int cstime, int starttime, int uptime);

#endif