/* 
 * CIS 415 Project 1
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "Project1.h"

struct PCB *cur_PCB;
int programme_cnt = 0, my_flag;

void SIGUSR1_HANDLER(int sgnl){
    my_flag = 1;
}

void MCP_FIRESIG(int sgnl){
    int i;
    for (i = 0; i < programme_cnt; i++){
        kill(cur_PCB[i].PID, sgnl);
    }
}

void MCP_WAIT(){
    // Parent waiting for all child processes to finish.
    int i;
    for (i = 0; i < programme_cnt; i++){
        wait(NULL);
    }
}

void MCP_LAUNCHER(){

    // Initialise variables.
    int i;
    
    // Start Parent Processs.
    for (i = 0; i < programme_cnt; i++){

        // Start Child Process.
        cur_PCB[i].PID = fork();

        // If error.
        if (cur_PCB[i].PID < 0){
            perror("ERROR OCCURED\n");
            exit(EXIT_FAILURE);
        }
        // Execute if forking is successful.
        if (cur_PCB[i].PID == 0) {
            while (my_flag == 0){
                usleep(1);
            }
            if(execvp(cur_PCB[i].cmd, cur_PCB[i].args) == -1){
                printf("!!! Bad Programme -> %s , exited.\n", cur_PCB[i].cmd);
                free_db();
            }
            exit(EXIT_SUCCESS);
        }
    }
}

void readsum(FILE *in_stream){

    // Create buffer and initialise sum of lines in file.
    char buffer[1024];
    
    // Sum calculation.
    while (fgets(buffer, sizeof(buffer), in_stream)){
        programme_cnt++;
    }
}

void readio(FILE *in_stream){

    // Initialise buffer and sum of lines in file.
    int i;
    char buffer[1024];
    int index = 0;

    // Read from stdin or file.
    for (i = 0; i < programme_cnt; i++){
        // Readline.
        fgets(buffer, sizeof(buffer), in_stream);
        // Make a copy to check line sum.
        char *line_check = strdup(buffer);
        char *sum_token = strtok(line_check, "\n\r ");
        // Insert the first argument into cmd holder.
        cur_PCB[i].cmd = strdup(sum_token);
        // Initialise inserting token into 2-D String array.
        while (sum_token != NULL){
            sum_token = strtok(NULL, "\n\r ");
            index++;
        }
        free(line_check);
        
        // Memory allocate the args.
        cur_PCB[i].args = malloc((index + 1) * sizeof(char *));
        cur_PCB[i].args[index] = NULL;
        cur_PCB[i].argc = index;
        cur_PCB[i].HasExited = 0;
        cur_PCB[i].State = NotStarted;
        cur_PCB[i].UserTime = 0;
        cur_PCB[i].SystemTime = 0;

        // Reset the index pointer.
        index = 0;

        // Initialise token for input.
        char *token = strtok(buffer, "\n\r ");
        // Initialise inserting token into 2-D String array.
        while (token != NULL){
            cur_PCB[i].args[index] = strdup(token);
            token = strtok(NULL, "\n\r ");
            index++;
        }
        // Reset the index pointer.
        index = 0;
    }
}

void malloc_lines(struct PCB **cur_PCB){
    // Malloc the List in PCB
    *cur_PCB = (struct PCB*)malloc(programme_cnt * sizeof(struct PCB));
}

void free_db(){

    // Rotating index initialisation
    int i, j;

    // Initiate rotation along array of PCB.
    for (i = 0; i < programme_cnt; i++){
        // Free the command.
        free(cur_PCB[i].cmd);
        for (j = 0; j < cur_PCB[i].argc; j++){
            // Free the arg strings.
            free(cur_PCB[i].args[j]);
        }
        // Free the arg array.
        free(cur_PCB[i].args);
    }
    // Free the PCB itself. (Skynet: I'M FREE!!!!!)
    free(cur_PCB);
}

int main(int argc, const char *argv[]){
    
    // Create File Objects and default to stdin and stdout.
    FILE *in_stream_io;
    in_stream_io = stdin;

    // Exit if bad parameters.
    if (argc > 2){
        fclose(in_stream_io);
        exit(0);
    }
    // If there is input parameter.
    if (argc > 1){
        in_stream_io = fopen(argv[1], "r");
    }
    //Check if files exists, if not, exit.
    if (in_stream_io == NULL){
        exit(0);
    }

    // Read line numbers and input.
    readsum(in_stream_io);

    // Rewind the file reader to beginning.
    rewind(in_stream_io);

    // Memory Allocate through sum of lines.
    malloc_lines(&cur_PCB);

    // Initialise transfer from file to database.
    readio(in_stream_io);

    // Close input streams for sum.
    fclose(in_stream_io);

    // Subscribing to SIGUSR1.
    signal(SIGUSR1, SIGUSR1_HANDLER);

    // Engage the launcher, fire, stop, proceed, and wait until exit.
    MCP_LAUNCHER();
    MCP_FIRESIG(SIGUSR1);
    MCP_FIRESIG(SIGSTOP);
    MCP_FIRESIG(SIGCONT);
    MCP_WAIT();

    // Freeing the database after use.
    free_db();

    // End.
    return 0;
}