/* 
 * CIS 415 Project 1
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work, except the following:
 *      Reading from proc:
 *          https://stackoverflow.com/questions/29991182/programmatically-read-all-the-processes-status-from-proc
 *      Proc man page:
 *          http://man7.org/linux/man-pages/man5/proc.5.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>

#include "Project1.h"

struct PCB *cur_PCB;
int current_programme = 0, programme_cnt = 0, custom_run = 0, custom_exit = 0;

void SIGALRM_HANDLER(int sgnl){

    // Halt current processes.
    if (cur_PCB[current_programme].HasExited == 1){
        cur_PCB[current_programme].State = Exited;
    } else {
        cur_PCB[current_programme].State = Paused;
        kill(cur_PCB[current_programme].PID, SIGSTOP);
    }

    // Exit if all processes are done.
    if (MCP_HASEXITED() == 1){
        kill(getpid(), SIGUSR2);
        return;
    }

    // Find next available procress.
    do{
        current_programme = (current_programme + 1) % programme_cnt;
    } while (cur_PCB[current_programme].State == Exited);

    // Initiate the updated current process.
    switch (cur_PCB[current_programme].State)
    {
    case Exited:
        MCP_INFO_MANAGER();
        break;
    case Running:
        cur_PCB[current_programme].State = Paused;
        kill(cur_PCB[current_programme].PID, SIGSTOP);
        MCP_INFO_MANAGER();
        break;
    case NotStarted:
        cur_PCB[current_programme].State = Running;
        kill(cur_PCB[current_programme].PID, SIGUSR1);
        MCP_INFO_MANAGER();
        break;
    case Paused:
        cur_PCB[current_programme].State = Running;
        kill(cur_PCB[current_programme].PID, SIGCONT);
        MCP_INFO_MANAGER();
        break;
    default:
        printf("This should not happen. REEEEEEEEEEEEEE\n");
        break;
    }
}

void SIGCHLD_HANDLER(int sgnl){
    int i;
    for (i = 0; i < programme_cnt; i++){
        int status;
        if (waitpid(cur_PCB[i].PID, &status, WNOHANG) > 0){
            if (WIFEXITED(status)){
                cur_PCB[i].HasExited = 1;
            }
        }
    }
}

void SIGUSR1_HANDLER(int sgnl){
    custom_run = 1;
}

void SIGUSR2_HANDLER(int sgnl){
    custom_exit = 1;
}

void MCP_TIMER(int quantum){

    // Create and set timer.
    struct itimerval timer;
    timer.it_value.tv_sec = (25 / 1000);
    timer.it_value.tv_usec = ((25 * 1000) % 1000000);
    timer.it_interval = timer.it_value;

    // Execute the first job.
    kill(cur_PCB[current_programme].PID, SIGUSR1);
    cur_PCB[current_programme].State = Running;

    // Initiate the timer for SIGALRM.
    setitimer (ITIMER_REAL, &timer, NULL);
}

int MCP_HASEXITED(){
    //Checks if all processes exited.
    int i;
    for (i = 0; i < programme_cnt; i++){
        if (cur_PCB[i].HasExited == 0){
            return 0;
        }
    }
    printf("ALL EXITED\n");
    return 1;
}

void MCP_WAIT(){
    // Parent waiting for all child processes to finish.
    sigset_t mask, oldmask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR2);
    sigprocmask (SIG_BLOCK, &mask, &oldmask);
    while (custom_exit == 0){
        sigsuspend (&oldmask);
    }
    sigprocmask (SIG_UNBLOCK, &mask, NULL);
}

void MCP_LAUNCHER(){

    // Initialise variables.
    int i;
    
    // Start Parent Processs.
    for (i = 0; i < programme_cnt; i++){

        // Start Child Process.
        cur_PCB[i].PID = fork();

        // If error.
        if (cur_PCB[i].PID < 0){
            perror("ERROR OCCURED\n");
            exit(EXIT_FAILURE);
        }
        // Execute if forking is successful.
        if (cur_PCB[i].PID == 0) {
            while (custom_run == 0){
                sleep(1);
            }
            if(execvp(cur_PCB[i].cmd, cur_PCB[i].args) == -1){
                printf("!!! Bad Programme -> %s , exited.\n", cur_PCB[i].cmd);
                free_db();
            }
            exit(EXIT_SUCCESS);
        }
    }
}

void MCP_INFO_MANAGER(){

    // Initialise variables.
    int i = 0;

    printf("PID\tState\tUTime\tSTime\tVMSize\tRSSize\n");

    for (i = 0; i < programme_cnt; i++){
        MCP_FETCH_INFO(cur_PCB[i].PID, i);
    }
    printf("\n");
}

void MCP_FETCH_INFO(int cur_pid, int procindex){

    // Initialise variables.
    int line = 0, index = 1, memsize;
    char dir[40], buffer[1024];
    FILE *FETCH_INFO;

    // proc/pid/stat
    sprintf(dir, "/proc/%d/stat", cur_pid);
    FETCH_INFO = fopen(dir, "r");
    if(!FETCH_INFO){
        goto Skipper;
    }
    printf("%d\t", cur_pid);
    while(fgets(buffer, 100, FETCH_INFO)){

        char *line_check = strdup(buffer);
        char *token = strtok(line_check, "\n\r ");

        while (token != NULL){
            token = strtok(NULL, "\n\r ");
            switch (index)
            {
            case 2:     // Status
                printf("%s\t", token);            
                break;
            case 13:    // User time
                printf("%s\t", token);
                break;
            case 14:    // System time
                printf("%s\t", token);
                break;
            case 22:    // Virtual Memory Size
                memsize = atof(token);
                char *voltype = "b";
                if (memsize >= 1000){
                    if (memsize >= 1000000){
                        memsize = memsize / 1000000;
                        voltype = "mb";
                    } else {
                        memsize = memsize / 1000;
                        voltype = "kb";
                    }
                }
                printf("%d%s\t", memsize, voltype);
                break;
            case 23:    // Resident Set Size
                printf("%s\t", token);
                break;
            default:
                break;
            }
            index++;
        }
        line++;
        free(line_check);
    }
    fclose(FETCH_INFO);
    printf("\n");
    Skipper:
    return;
}

void readsum(FILE *in_stream){

    // Create buffer and initialise sum of lines in file.
    char buffer[1024];
    
    // Sum calculation.
    while (fgets(buffer, sizeof(buffer), in_stream)){
        programme_cnt++;
    }
}

void readio(FILE *in_stream){

    // Initialise buffer and sum of lines in file.
    int i;
    char buffer[1024];
    int index = 0;

    // Read from stdin or file.
    for (i = 0; i < programme_cnt; i++){
        // Readline.
        fgets(buffer, sizeof(buffer), in_stream);
        // Make a copy to check line sum.
        char *line_check = strdup(buffer);
        char *sum_token = strtok(line_check, "\n\r ");
        // Insert the first argument into cmd holder.
        cur_PCB[i].cmd = strdup(sum_token);
        // Initialise inserting token into 2-D String array.
        while (sum_token != NULL){
            sum_token = strtok(NULL, "\n\r ");
            index++;
        }
        free(line_check);
        
        // Memory allocate the args.
        cur_PCB[i].args = malloc((index + 1) * sizeof(char *));
        cur_PCB[i].args[index] = NULL;
        cur_PCB[i].argc = index;
        cur_PCB[i].HasExited = 0;
        cur_PCB[i].State = NotStarted;
        cur_PCB[i].UserTime = 0;
        cur_PCB[i].SystemTime = 0;

        // Reset the index pointer.
        index = 0;

        // Initialise token for input.
        char *token = strtok(buffer, "\n\r ");
        // Initialise inserting token into 2-D String array.
        while (token != NULL){
            cur_PCB[i].args[index] = strdup(token);
            token = strtok(NULL, "\n\r ");
            index++;
        }
        // Reset the index pointer.
        index = 0;
    }
}

void malloc_lines(struct PCB **cur_PCB){
    // Malloc the List in PCB
    *cur_PCB = (struct PCB*)malloc(programme_cnt * sizeof(struct PCB));
}

void free_db(){

    // Rotating index initialisation
    int i, j;

    // Initiate rotation along array of PCB.
    for (i = 0; i < programme_cnt; i++){
        // Free the command.
        free(cur_PCB[i].cmd);
        for (j = 0; j < cur_PCB[i].argc; j++){
            // Free the arg strings.
            free(cur_PCB[i].args[j]);
        }
        // Free the arg array.
        free(cur_PCB[i].args);
    }
    // Free the PCB itself. (Skynet: I'M FREE!!!!!)
    free(cur_PCB);
}

int main(int argc, const char *argv[]){
    
    // Create File Objects and default to stdin and stdout.
    FILE *in_stream_io;
    in_stream_io = stdin;

    // Exit if bad parameters.
    if (argc > 2){
        fclose(in_stream_io);
        exit(0);
    }
    // If there is input parameter.
    if (argc > 1){
        in_stream_io = fopen(argv[1], "r");
    }
    //Check if files exists, if not, exit.
    if (in_stream_io == NULL){
        exit(0);
    }

    // Read line numbers and input.
    readsum(in_stream_io);

    // Rewind the file reader to beginning.
    rewind(in_stream_io);

    // Memory Allocate through sum of lines.
    malloc_lines(&cur_PCB);

    // Initialise transfer from file to database.
    readio(in_stream_io);

    // Close input streams for sum.
    fclose(in_stream_io);

    // Subscribing to SIGALRM, SIGCHLD, SIGUSR2.
    signal(SIGALRM, SIGALRM_HANDLER);
    signal(SIGCHLD, SIGCHLD_HANDLER);
    signal(SIGUSR1, SIGUSR1_HANDLER);
    signal(SIGUSR2, SIGUSR2_HANDLER);

    // Engage the launcher, and wait until exit.
    MCP_LAUNCHER();
    MCP_TIMER(1);
    MCP_WAIT();

    // Freeing the database after use.
    free_db();

    // End.
    return 0;
}