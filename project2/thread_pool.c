/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "utilities.h"
#include "Project2.h"

int global_lock = 0, is_html = 0, pub_checkpoint = 0, clean_checkpoint = 0;
FILE *sub_html;

long long get_entry_pter[MAXTOPICQUEUES] = {0};
extern TopicQueue TQ_array[];
extern int TQ_index, pub_cnt, sub_cnt;

void *pub_pt(void *argument){
    char *arg_parsed = (char *)argument;
    fprintf(stderr, "Current publisher thread ID is:\t\t%ld\t|\tArgument: %s\n", (long) pthread_self(), arg_parsed);
    // Awaiting unlock to run.
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());
    int i = 0, j = 0, position_index = 0, desired_tq_raw = 0, desired_tq = 0;
    struct TopicEntry publisher = {"", ""};
    struct FileLines *pub = LoadAFile(arg_parsed);
    for (i = 0; i < pub->LineCount; i++){
        switch (position_index){
            case 0:
                desired_tq_raw = atoi(pub->Lines[i]);
                publisher.pubID = desired_tq_raw;
                for (j = 0; j < TQ_index; j++){
                    if (TQ_array[j].ID == desired_tq_raw){
                        desired_tq = j;
                        break;
                    }
                }
                position_index++;
                break;
            case 1:
                publisher.photoURL = pub->Lines[i];
                position_index++;
                break;
            case 2:
                publisher.photoCaption = pub->Lines[i];
                position_index++;
                break;
            case 3:
                while(enqueue(&(TQ_array[desired_tq]), publisher) != 1){
                    sleep(1);
                }
                sleep(atoi(pub->Lines[i]));
                position_index = 0;  
                break;     
        }
    }
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    FreeFile(pub);
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void *sub_pt(void *argument){
    char *arg_parsed = (char *)argument;
    fprintf(stderr, "Current subscriber thread ID is:\t%ld\t|\tArgument: %s\n", (long) pthread_self(), arg_parsed);
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());
    if (is_html == 1){
        char fname[1024];
        snprintf(fname, 1024, "HTML/%ld.html", (long)pthread_self);
        sub_html = fopen(fname, "w");
        fprintf(sub_html, "<!DOCTYPE html>\n");
        fprintf(sub_html, "<html>\n");
        fprintf(sub_html, "<head>\n");
        fprintf(sub_html, "	<title>InstaQuack</title>\n");
        fprintf(sub_html, "	<style>\n");
        fprintf(sub_html, "		.a {border: 1px solid black; margin: 4px; display: inline-block}\n");
        fprintf(sub_html, "		.b {text-align: center}\n");
        fprintf(sub_html, "		img {width: 240px; height: auto}\n");
        fprintf(sub_html, "		p, h1, h2 {text-align: center}\n");
        fprintf(sub_html, "	</style>\n");
        fprintf(sub_html, "</head>\n");
        fprintf(sub_html, "<body>\n");
        fprintf(sub_html, "	<h1>InstaQuack (Subscriber)</h1>");
    }
    int i = 0, j = 0, position_index = 0, desired_tq_raw = 0, desired_tq = 0;
    struct TopicEntry retriever = {"", ""};
    struct FileLines *sub = LoadAFile(arg_parsed);
    for (i = 0; i < sub->LineCount; i++){
        switch (position_index){
            case 0:
                desired_tq_raw = atoi(sub->Lines[i]);
                for (j = 0; j < TQ_index; j++){
                    if (TQ_array[j].ID == desired_tq_raw){
                        desired_tq = j;
                        break;
                    }
                }
                get_entry_pter[desired_tq] = getentry(&(TQ_array[desired_tq]), get_entry_pter[desired_tq], &retriever) + 1;
                while (get_entry_pter[desired_tq] <= 0){
                    sleep(1);
                    get_entry_pter[desired_tq] = getentry(&(TQ_array[desired_tq]), get_entry_pter[desired_tq], &retriever) + 1;
                    if (pub_checkpoint == 1){
                        goto Sub_Term;
                    }
                }
                fprintf(stderr, "%d\n%s\n%s\n", retriever.pubID, retriever.photoURL, retriever.photoCaption);
                if (is_html == 1){
                    fprintf(sub_html, "	<h2>Topic: %d</h2>\n", retriever.pubID);
                    fprintf(sub_html, "	<div class='b'>\n");
                    fprintf(sub_html, "		<div class='a'>\n");
                    fprintf(sub_html, "			<img src=%s>\n", retriever.photoURL);
                    fprintf(sub_html, "			<p>%s</p>\n", retriever.photoCaption);
                    fprintf(sub_html, "		</div>\n");
                    fprintf(sub_html, "	</div>\n");
                }
                position_index++;
                break;
            case 1:
                sleep(atoi(sub->Lines[i]));
                position_index = 0;  
                break;     
        }
    }
    Sub_Term:
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    if (is_html == 1){
        fprintf(sub_html, "</body>\n");
        fprintf(sub_html, "</html>");
        fclose(sub_html);
    }
    FreeFile(sub);
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void *cl_pt(void *argument){
    int DELTA = *((int *)argument);
    fprintf(stderr, "Current cleanup thread ID is:\t\t%ld\t|\tArgument: %d\n", (long) pthread_self(), DELTA);
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());

    int i = 0;
    struct timeval cleanup;
    
    sleep(2);

    while(clean_checkpoint == 0){

        for (i = 0; i < TQ_index; i++){

            gettimeofday(&cleanup, NULL);
            cleanup.tv_sec -= DELTA;

            if (TS_BB_IsEmpty(TQ_array[i].ts_bq) == 0){
                struct TopicEntry forCleanup = {"",""};
                getentry(&(TQ_array[i]), 0, &forCleanup);

                if (cleanup.tv_sec > forCleanup.timestamp.tv_sec){
                    dequeue(&(TQ_array[i]));
                }
                else if (cleanup.tv_sec == forCleanup.timestamp.tv_sec){
                    if (cleanup.tv_usec >= forCleanup.timestamp.tv_usec){
                        dequeue(&(TQ_array[i]));
                    }
                }
            }
        }
        sleep(CLEANUP_TIME);
    }
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void THREADS_FIRE(){
    global_lock = 1;
}

void CLEAN_EXIT(){
    clean_checkpoint = 1;
}

void PUB_EXIT(){
    pub_checkpoint = 1;
}

void subscriber_html(){
    mkdir("HTML", ACCESSPERMS); 
    is_html = 1;
}