/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "Project2.h"

int main(int argc, char* argv[]){
    pthread_t publisher, subscriber, cleanup;
    int cp_int = 1145141919;
    pthread_create(&publisher, NULL, pub_pt_TEST, "This is publisher");
    pthread_create(&subscriber, NULL, sub_pt_TEST, "This is subscriber");
    pthread_create(&cleanup, NULL, cl_pt_TEST, &cp_int);
    THREADS_FIRE_TEST();
    pthread_join(publisher, NULL);
    pthread_join(subscriber, NULL);
    pthread_join(cleanup, NULL);
}