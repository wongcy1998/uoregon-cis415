/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "Project2.h"

int global_lock_test = 0;

void *pub_pt_TEST(void *argument){
    char *arg_parsed = (char *)argument;
    fprintf(stderr, "Current publisher thread ID is:\t\t%ld\t|\tArgument: %s\n", (long) pthread_self(), arg_parsed);
    while(global_lock_test == 0){
        sleep(1);
    }
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    fprintf(stderr, "Publisher\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void *sub_pt_TEST(void *argument){
    char *arg_parsed = (char *)argument;
    fprintf(stderr, "Current subscriber thread ID is:\t%ld\t|\tArgument: %s\n", (long) pthread_self(), arg_parsed);
    while(global_lock_test == 0){
        sleep(1);
    }
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    fprintf(stderr, "Subscriber\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void *cl_pt_TEST(void *argument){
    int DELTA = *((int *)argument);
    fprintf(stderr, "Current cleanup thread ID is:\t\t%ld\t|\tArgument: %d\n", (long) pthread_self(), DELTA);
    while(global_lock_test == 0){
        sleep(1);
    }
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Running <!>\n", (long) pthread_self());
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Exiting <!>\n", (long) pthread_self());
    fprintf(stderr, "Cleanup\t\t\t\t\t%ld |\t<!> Exited <!>\n", (long) pthread_self());
    return NULL;
}

void THREADS_FIRE_TEST(){
    global_lock_test = 1;
}