/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file is modified from the version provided by instructors.
 */

#include <stdio.h>
#include <stdlib.h>

#include "bounded_queue.h"
#include "Project2.h"

struct bounded_queue
{
        int size;       // capacity
        void **buffer;  // storage
        long long head; // 
        long long tail; //
};

int RoundIDToBufferIndex(int size, long long index)
{
        long long value = (index % ((long long)size));
        return (int)value;
}

BoundedQueue *BB_MallocBoundedQueue(long size)
{
        struct bounded_queue *returnValue = malloc(sizeof(struct bounded_queue));
        returnValue->buffer = malloc(size * sizeof(void*));
        returnValue->size = size;
        returnValue->head = 0;
        returnValue->tail = 0;
        return (BoundedQueue *)returnValue;
}

long long BB_TryEnqueue(struct bounded_queue *queue,void *item)
{
        long long retVal = -1;
        if (BB_IsFull(queue) == 1){
                return retVal;
        } else {
                int index = RoundIDToBufferIndex(queue->size, queue->head);
                retVal = queue->head;
                queue->buffer[index] = item;
                queue->head += 1;
                return retVal;
        }
}

int BB_TryDequeue(struct bounded_queue *queue,long long id)
{
        if ((BB_IsEmpty(queue) == 1) || (id != queue->tail)){
                return 0;
        } else {
                int index = RoundIDToBufferIndex(queue->size, id);
                queue->buffer[index] = NULL;
                queue->tail += 1;
                return 1;
        }
}

long long BB_GetFront(struct bounded_queue *queue)
{
        return queue->head;
}

long long BB_GetBack(struct bounded_queue *queue)
{
        return queue->tail;
}

int BB_GetCount(struct bounded_queue *queue)
{
        long long returnValue = queue->head - queue->tail;
        return (int)returnValue;
}

int BB_IsIdValid(struct bounded_queue *queue,long long id)
{
        if ((id >= queue->tail) && (id < queue->head) && (BB_IsEmpty(queue) == 0)){
                return 1;
        }
        return 0;
}

void *BB_GetItem(struct bounded_queue *queue,long long id)
{
        void *returnValue = NULL;
        if (BB_IsIdValid(queue, id) == 1){
                int index = RoundIDToBufferIndex(queue->size, id);
                returnValue = queue->buffer[index];
        }
        return returnValue;
}

int BB_IsFull(struct bounded_queue *queue)
{
        if ((queue->head - queue->tail) == queue->size){
                return 1;
        }
        return 0;
}

int BB_IsEmpty(struct bounded_queue *queue)
{
        if (queue->head == queue->tail){
                return 1;
        }
        return 0;
}

void BB_FreeBoundedQueue(struct bounded_queue *queue)
{
        free(queue->buffer);
        free(queue);
}

