#ifndef PROJECT2_H
#define PROJECT2_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>

#include "thread_safe_bounded_queue.h"

#define MAXTOPICS 10
#define MAXTOPICQUEUES 10
#define MAXENTRIES 10000
#define MAXPROXIES 10
#define CLEANUP_TIME 1

///////////
// PART1 //
///////////

typedef struct topic_queue TopicQueue;

struct topic_queue {
    TSBoundedQueue *ts_bq;
    long long entry_counter;
    int ID;
    int size;
    char *Name;
};

struct TopicEntry {
    char *photoURL;
    char *photoCaption;
    int entrynum;
    struct timeval timestamp;
    int pubID;
};

void initQueue(TopicQueue *queue, int size);

int enqueue(TopicQueue *queue, struct TopicEntry item);

int dequeue(TopicQueue *queue);

long long getentry(TopicQueue *queue, long long ID, struct TopicEntry *entry);

int print_queue(TopicQueue *queue);

void fill_queue(TopicQueue *queue);

int empty_queue(TopicQueue *queue);

void delete_topic_queue(TopicQueue *queue);

///////////
// PART2 //
///////////

void *pub_pt_TEST(void *arg);

void *sub_pt_TEST(void *arg);

void *cl_pt_TEST(void *arg);

void THREADS_FIRE_TEST();

///////////
// PART4 //
///////////

void *pub_pt(void *arg);

void *sub_pt(void *arg);

void *cl_pt(void *arg);

void THREADS_FIRE();

void CLEAN_EXIT();

void PUB_EXIT();

///////////
// PART5 //
///////////

void subscriber_html();

#endif