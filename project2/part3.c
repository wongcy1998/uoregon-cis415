/* 
 * CIS 415 Project 2
 * Author: Toby Wong
 * ID: chunyauw
 * 
 * This file entirely is my own work.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>

#include "Project2.h"
#include "p1fxns.h"

TopicQueue TQ_array[MAXTOPICQUEUES];
int TQ_index = 0, pub_cnt = 0, sub_cnt = 0;

int main(int argc, char* argv[]){

    FILE *in_stream_io;
    in_stream_io = stdin;

    // Exit if bad parameters.
    if (argc > 2){
        fclose(in_stream_io);
        exit(0);
    }
    // If there is input parameter.
    if (argc > 1){
        in_stream_io = fopen(argv[1], "r");
    }
    //Check if files exists, if not, exit.
    if (in_stream_io == NULL){
        exit(0);
    }

    char buffer[1024], ordinance[1024];
    int i = 0;

    int DELTA = 0;

    char *pubfiles[MAXPROXIES], *subfiles[MAXPROXIES];
    pthread_t publishers[MAXPROXIES], subscribers[MAXPROXIES], cleanup;

    while (fgets(buffer, sizeof(buffer), in_stream_io)){
        int index = 0;
        // For Linux based stripping newline.
        strtok(buffer, "\n");
        // For Windows stripping return.
        strtok(buffer, "\r");

        while (index != -1){

            index = p1getword(buffer, index, ordinance);

            if (index != -1){
                // COMMANDS TO CREATE
                if (strcmp(ordinance, "create") == 0){
                    // COMMANDS TO CREATE TOPIC QUEUE
                    fprintf(stderr, "Creating Topic Queue\n");
                    index = p1getword(buffer, index, ordinance);
                    index = p1getword(buffer, index, ordinance);
                    if(TQ_index < MAXTOPICQUEUES){
                        TQ_array[TQ_index].ID = p1atoi(ordinance);
                    } else {fprintf(stderr, "Reached Maximum amount of Queues.\n");}
                    index = p1getword(buffer, index, ordinance);
                    if(TQ_index < MAXTOPICQUEUES){
                        TQ_array[TQ_index].Name = p1strdup(ordinance);
                    }
                    index = p1getword(buffer, index, ordinance);
                    if(TQ_index < MAXTOPICQUEUES){
                        initQueue(&(TQ_array[TQ_index]), p1atoi(ordinance));
                        fprintf(stderr, "Topic Queue Created at index %d\n", TQ_index);
                        TQ_index += 1;
                    }
                    index = p1getword(buffer, index, ordinance);
                }
                // COMMANDS TO QUERY
                else if (strcmp(ordinance, "query") == 0){
                    index = p1getword(buffer, index, ordinance);
                    // COMMANDS TO QUERY TOPICS
                    if (strcmp(ordinance, "topics") == 0){
                        fprintf(stderr, "Printing Query on Topics:\n");
                        for (i = 0; i < TQ_index; i++){
                            fprintf(stderr, "\tID: %d\t|\tLength: %d\n", TQ_array[i].ID, TQ_array[i].size);
                        }
                        index = p1getword(buffer, index, ordinance);
                    }
                    // COMMANDS TO QUERY PUBLISHERS
                    else if (strcmp(ordinance, "publishers") == 0){
                        fprintf(stderr, "Printing Query on Publishers:\n");
                        for (i = 0; i < pub_cnt; i++){
                            fprintf(stderr, "\tID: %ld\t|\tFile: %s\n", publishers[i], pubfiles[i]);
                        }
                    }
                    // COMMANDS TO QUERY SUBSCRIBERS
                    else if (strcmp(ordinance, "subscribers") == 0){
                        fprintf(stderr, "Printing Query on Subscribers:\n");
                        for (i = 0; i < sub_cnt; i++){
                            fprintf(stderr, "\tID: %ld\t|\tFile: %s\n", subscribers[i], subfiles[i]);
                        }
                    }
                }
                // COMMANDS TO ADD
                else if (strcmp(ordinance, "add") == 0){
                    index = p1getword(buffer, index, ordinance);
                    // COMMANDS TO ADD PUBLISHERS
                    if (strcmp(ordinance, "publisher") == 0){
                        fprintf(stderr, "Adding publisher\n");
                        index = p1getword(buffer, index, ordinance);
                        if (pub_cnt < MAXPROXIES){
                            pubfiles[pub_cnt] = p1strdup(ordinance);
                            pthread_create(&publishers[pub_cnt], NULL, pub_pt_TEST, pubfiles[pub_cnt]);
                            pub_cnt += 1;
                        } else {fprintf(stderr, "Reached Maximum amount of Publishers.\n");}
                    }
                    // COMMANDS TO ADD SUBSCRIBERS
                    else if (strcmp(ordinance, "subscriber") == 0){
                        fprintf(stderr, "Adding subscriber\n");
                        index = p1getword(buffer, index, ordinance);
                        if (sub_cnt < MAXPROXIES){
                            subfiles[sub_cnt] = p1strdup(ordinance);
                            pthread_create(&subscribers[sub_cnt], NULL, sub_pt_TEST, subfiles[sub_cnt]);
                            sub_cnt += 1;
                        } else {fprintf(stderr, "Reached Maximum amount of Subscribers.\n");}
                    }
                    index = p1getword(buffer, index, ordinance);
                }
                // COMMANDS TO CHANGE DELTA
                else if (strcmp(ordinance, "delta") == 0){
                    index = p1getword(buffer, index, ordinance);
                    DELTA = p1atoi(ordinance);
                    fprintf(stderr, "DELTA has been set to: %d\n", DELTA);
                }
                // COMMANDS TO START
                else if (strcmp(ordinance, "start") == 0){
                    fprintf(stderr, "INITIALISING START\n");
                    pthread_create(&cleanup, NULL, cl_pt_TEST, &DELTA);
                    THREADS_FIRE_TEST();
                    pthread_join(cleanup, NULL);
                    for (i = 0; i < pub_cnt; i++){
                        pthread_join(publishers[i], NULL);
                    }
                    for (i = 0; i < sub_cnt; i++){
                        pthread_join(subscribers[i], NULL);
                    }
                }
            }
        }
    }

    for (i = 0; i < TQ_index; i++){
        free(TQ_array[i].Name);
        delete_topic_queue(&(TQ_array[i]));
    }
    
    for (i = 0; i < pub_cnt; i++){
        free(pubfiles[i]);
    }
    
    for (i = 0; i < sub_cnt; i++){
        free(subfiles[i]);
    }

    fclose(in_stream_io);
    return 0;
}